﻿using Iot.Device.Ws28xx;
using System;
using System.Device.Spi;
using System.Drawing;
using System.Threading;

namespace dotnetcore_ws2812
{
    class Program
    {

        private const int LED_COUNT = 10;

        static void Main(string[] args)
        {
            Console.WriteLine("Started");

            //setup the pin out
            var settings = new SpiConnectionSettings(0, 0)
            {
                ClockFrequency = 2_400_000,
                Mode = SpiMode.Mode0,
                DataBitLength = 8
            };

            //create the leds on the pin
            var spi = SpiDevice.Create(settings);
            var leds = new Ws2812b(spi, LED_COUNT);

            //grab the "image" being displayed
            var image = leds.Image;
            image.Clear();

            //work with yellow
            var color = Color.Yellow;
            //loop forever
            while (true)
            {
                image.Clear(color);
                //update the leds
                leds.Update();
                //sleep for a second
                Thread.Sleep(1000);
                //change color from yellow to blue
                if (color == Color.Yellow)
                    color = Color.Blue;
                else
                    color = Color.Yellow;
            }
        }
    }
}
